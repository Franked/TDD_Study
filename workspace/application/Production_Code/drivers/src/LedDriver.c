/*
 * LedDriver.c
 *
 *  Created on: Dec 5, 2017
 *      Author: user
 */

#include "LedDriver.h"

static uint16_t * ledsAddress;
static uint16_t ledsImage;

static void updateHardware(void) {
	*ledsAddress = ledsImage;
}

enum {
	FIRST_LED = 0, LAST_LED = 16
};

static bool IsLedOutOfBounds(int ledNumber) {
	if (!inline_IsUnsignedInRange(ledNumber, FIRST_LED, LAST_LED)) {
		RUNTIME_ERROR("LED Driver: out-of-bounds LED", ledNumber);
		return true;
	}
	return false;
}
void LedDriver_Create(uint16_t * reg) {
	ledsAddress = reg;
	ledsImage = 0x0000;
	updateHardware();
}

void LedDriver_TurnOn(uint16_t ledNumber) {
	if (IsLedOutOfBounds(ledNumber))
		return;
	ledsImage |= NUM_TO_BIT(inline_NumToBitNum(ledNumber, Short));
	updateHardware();
}

void LedDriver_TurnOff(uint16_t ledNumber) {
	if (IsLedOutOfBounds(ledNumber))
		return;
	ledsImage &= ~NUM_TO_BIT(inline_NumToBitNum(ledNumber, Short));
	updateHardware();
}

void LedDriver_TurnAllOn(void) {
	ledsImage = UINT16_MAX;
	updateHardware();
}

void LedDriver_TurnAllOff(void) {
	ledsImage = 0x0000;
	updateHardware();
}
