/*
 * LedDriver.h
 *
 *  Created on: Dec 5, 2017
 *      Author: user
 */

#ifndef CUT_INC_LEDDRIVER_H_
#define CUT_INC_LEDDRIVER_H_

#include "stdint.h"
#include "stdbool.h"
#include "RuntimeError.h"
#include "helpers.h"

void LedDriver_Create(uint16_t * reg);
void LedDriver_TurnOn(uint16_t ledNumber);
void LedDriver_TurnOff(uint16_t ledNumber);
void LedDriver_TurnAllOn(void);
void LedDriver_TurnAllOff(void);
#endif /* CUT_INC_LEDDRIVER_H_ */
