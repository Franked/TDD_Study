/*
 * LightScheduler.c
 *
 *  Created on: Dec 14, 2017
 *      Author: Ehud Frank
 */

#include "LightScheduler.h"

typedef enum {
	TURN_ON, TURN_OFF
} LightEvent;

typedef struct {
	int id;
	int minuteOfDay;
	Day dayOfWeek;
	LightEvent event;

} ScheduledLightEvent;

static ScheduledLightEvent scheduledEvent;
static ScheduledLightEvent scheduledEvents[MAX_NUM_OF_LIGHTS];
static uint8_t scheduledEventsIndex;

static LightSchedulerStatus_t scheduleEvent(int id, Day day, int minuteOfDay,
		LightEvent event) {
	if (scheduledEventsIndex >= MAX_NUM_OF_LIGHTS) {
		return LS_ERROR;
	}
	scheduledEvents[scheduledEventsIndex].id = id;
	scheduledEvents[scheduledEventsIndex].minuteOfDay = minuteOfDay;
	scheduledEvents[scheduledEventsIndex].dayOfWeek = day;
	scheduledEvents[scheduledEventsIndex++].event = event;
	return LS_OK;
}

static bool LightScheduler_IsDayMatch(Day today, Day setDay) {
	switch (setDay) {
	case EVERYDAY:
		return true;
		break;
	case WEEKEND:
		if (today == SATURDAY || today == SUNDAY) {
			return true;
		}
		break;
	case WEEKDAY:
		if (!(today == SATURDAY || today == SUNDAY)) {
			return true;
		}
		break;
	default:
		if (today == setDay) {
			return true;
		}
		break;
	}
	return false;
}
static void operateLight(uint8_t index) {
	if (scheduledEvents[index].event == TURN_ON)
		LightController_On(scheduledEvents[index].id);
	else if (scheduledEvents[index].event == TURN_OFF)
		LightController_Off(scheduledEvents[index].id);
}

static void processEventDueNow(Time * time) {
	int i;
	for (i = 0; i < scheduledEventsIndex; i++) {
		if (scheduledEvents[i].id == UNUSED)
			continue;
		if (!LightScheduler_IsDayMatch(time->dayOfWeek,
				scheduledEvents[i].dayOfWeek)) {
			continue;
		}
		if (scheduledEvents[i].minuteOfDay != time->minuteOfDay)
			continue;

		operateLight(i);
	}
}

void LightScheduler_Create(void) {
	scheduledEventsIndex = 0;
	scheduledEvent.id = UNUSED;
	int i;
	for (i = 0; i < MAX_NUM_OF_LIGHTS; i++) {
		scheduledEvents[i].id = UNUSED;
	}
	TimeService_InitTimer(60, LightScheduler_Wakeup);
}
void LightScheduler_Destroy(void) {
	scheduledEventsIndex = 0;
	scheduledEvent.id = UNUSED;
	int i;
	for (i = 0; i < MAX_NUM_OF_LIGHTS; i++) {
		scheduledEvents[i].id = UNUSED;
	}
	TimeService_DeinitTimer(60, LightScheduler_Wakeup);
}

LightSchedulerStatus_t LightScheduler_ScheduleTurnOn(int id, Day day,
		int minuteOfDay) {
	return scheduleEvent(id, day, minuteOfDay, TURN_ON);
}

LightSchedulerStatus_t LightScheduler_ScheduleTurnOff(int id, Day day,
		int minuteOfDay) {
	return scheduleEvent(id, day, minuteOfDay, TURN_OFF);
}

LightSchedulerStatus_t LightScheduler_ScheduleRemove(void) {
	if (scheduledEventsIndex) {
		scheduledEvents[--scheduledEventsIndex].id = UNUSED;
		return LS_OK;
	}
	return LS_ERROR;
}
void LightScheduler_Wakeup(void) {
	Time time;
	TimeService_GetTime(&time);

	processEventDueNow(&time);
}

void LightScheduler_Randomize(int id, Day day, int minuteOfDay) {
	int i;
	for (i = 0; i < scheduledEventsIndex; i++) {
		if (scheduledEvents[i].id == id && scheduledEvents[i].dayOfWeek == day
				&& scheduledEvents[i].minuteOfDay == minuteOfDay) {
			scheduledEvents[i].minuteOfDay += RandomMinute_Get();
		}
	}

}
