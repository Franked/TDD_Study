/*
 * RandomMinute.c
 *
 *  Created on: Dec 18, 2017
 *      Author: Ehud Frank
 */

#include "RandomMinute.h"

static int bound;

void RandomMinute_Create(int b) {
	bound = b;
}

//int RandomMinute_Get(void) {
//	return bound - rand() % (bound * 2 + 1);
//}

int RandomMinute_GetImpl(void)
{
return bound - rand() % (bound * 2 + 1);
}
int (*RandomMinute_Get)(void) = RandomMinute_GetImpl;
