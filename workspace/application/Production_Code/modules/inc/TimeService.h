/*
 * TimeService.h
 *
 *  Created on: Dec 15, 2017
 *      Author: user
 */

#ifndef PRODUCTION_CODE_MODULES_INC_TIMESERVICE_H_
#define PRODUCTION_CODE_MODULES_INC_TIMESERVICE_H_

#include "stdint.h"

enum {
	TIME_UNKNOWN = -1,
};
typedef struct Time {
	int minuteOfDay;
	int dayOfWeek;
} Time;

typedef void (*TimeServiceCallBack_t) (void);

typedef struct {
	TimeServiceCallBack_t cb;
	uint32_t period;
}TimeServiceTime_t;

void TimeService_GetTime(Time *time);
void TimeService_InitTimer(uint32_t period, TimeServiceCallBack_t cb);
void TimeService_DeinitTimer(uint32_t period, TimeServiceCallBack_t cb);

#endif /* PRODUCTION_CODE_MODULES_INC_TIMESERVICE_H_ */
