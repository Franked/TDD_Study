/*
 * LightScheduler.h
 *
 *  Created on: Dec 14, 2017
 *      Author: Ehud Frank
 */

#ifndef PRODUCTION_CODE_MODULES_INC_LIGHTSCHEDULER_H_
#define PRODUCTION_CODE_MODULES_INC_LIGHTSCHEDULER_H_

#include "stdint.h"
#include "stdbool.h"
#include "TimeService.h"
#include "LightController.h"
#include "RandomMinute.h"

typedef enum {
	LS_OK, LS_ERROR
} LightSchedulerStatus_t;

typedef enum {
	NONE = -1,
	EVERYDAY = 10,
	WEEKDAY,
	WEEKEND,
	SUNDAY = 1,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY
} Day;

enum {
	UNUSED
};
void LightScheduler_Create(void);
void LightScheduler_Destroy(void);
LightSchedulerStatus_t LightScheduler_ScheduleTurnOn(int id, Day day, int minuteOfDay);
LightSchedulerStatus_t LightScheduler_ScheduleTurnOff(int id, Day day, int minuteOfDay);
LightSchedulerStatus_t LightScheduler_ScheduleRemove(void);
void LightScheduler_Wakeup(void);
void LightScheduler_Randomize(int id, Day day,
		int minuteOfDay);

#endif /* PRODUCTION_CODE_MODULES_INC_LIGHTSCHEDULER_H_ */
