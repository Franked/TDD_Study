/*
 * LightController.h
 *
 *  Created on: Dec 14, 2017
 *      Author: user
 */

#ifndef PRODUCTION_CODE_DRIVERS_INC_LIGHTCONTROLLER_H_
#define PRODUCTION_CODE_DRIVERS_INC_LIGHTCONTROLLER_H_

#define MAX_NUM_OF_LIGHTS	128

void LightController_Create(void);
void LightController_Destroy(void);
void LightController_On(int id);
void LightController_Off(int id);

#endif /* PRODUCTION_CODE_DRIVERS_INC_LIGHTCONTROLLER_H_ */
