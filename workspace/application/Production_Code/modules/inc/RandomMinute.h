/*
 * RandomMinute.h
 *
 *  Created on: Dec 18, 2017
 *      Author: Ehud Frank
 */

#ifndef PRODUCTION_CODE_MODULES_INC_RANDOMMINUTE_H_
#define PRODUCTION_CODE_MODULES_INC_RANDOMMINUTE_H_

#include <stdlib.h>

void RandomMinute_Create(int b);
extern int (*RandomMinute_Get)(void);

#endif /* PRODUCTION_CODE_MODULES_INC_RANDOMMINUTE_H_ */
