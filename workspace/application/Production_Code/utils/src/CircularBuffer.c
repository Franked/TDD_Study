/*
 * CircularBuffer.c
 *
 *  Created on: Dec 6, 2017
 *      Author: Ehud Frank
 */

#include "CircularBuffer.h"

bool CircularBuffer_Push(circularBuffer_t * circBuff, uint8_t data) {
	if (circBuff->count == circBuff->length) {
		return CIRC_BUFF_NAK;
	}
	circBuff->data[circBuff->index++] = data;
	circBuff->count++;
	if (circBuff->index >= circBuff->length) {
		circBuff->index = circBuff->length;
	}
	return CIRC_BUFF_AK;
}

uint8_t CircularBuffer_Pop(circularBuffer_t * circBuff) {
	if (!circBuff->count) {
		return circBuff->data[circBuff->outdex];
	}
	circBuff->count--;
	return circBuff->data[circBuff->outdex++];
}

void CircularBuffer_Clear(circularBuffer_t * circBuff) {
	circBuff->index = circBuff->outdex = 0;
	circBuff->count = 0;
}

bool CircularBuffer_IsEmpty(circularBuffer_t * circBuff) {
	return circBuff->index == circBuff->outdex;
}
bool CircularBuffer_IsFull(circularBuffer_t * circBuff) {
	return circBuff->count == circBuff->length;
}
