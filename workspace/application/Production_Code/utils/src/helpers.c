/*
 * helpers.c
 *
 *  Created on: Dec 8, 2017
 *      Author: Ehud Frank
 */

#include "helpers.h"

uint8_t NumToBitNum(int8_t num, data_type_t type) {
	uint8_t maxNumOfBits = 0;
	switch (type) {
	case Byte:
		maxNumOfBits = BYTE_BIT_SIZE;
		break;
	case Short:
		maxNumOfBits = SHORT_BIT_SIZE;
		break;
	case Long:
		maxNumOfBits = LONG_BIT_SIZE;
		break;
	}
	if (num <= 0) {
		return 0;
	}
	if (maxNumOfBits < num) {
		return maxNumOfBits - 1;
	}
	return NUM_TO_BIT_NUM(num);
}

bool IsUnsignedInRange(uint32_t num, uint32_t lowBound, uint32_t highBound) {
	return IS_IN_RANGE(num, lowBound, highBound);
}

bool IsSignedInRange(int32_t num, int32_t lowBound, int32_t highBound) {
	return IS_IN_RANGE(num, lowBound, highBound);
}
