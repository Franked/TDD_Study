/*
 * helpers.h
 *
 *  Created on: Dec 8, 2017
 *      Author: Ehud Frank
 */

#ifndef PRODUCTION_CODE_UTILS_INC_HELPERS_H_
#define PRODUCTION_CODE_UTILS_INC_HELPERS_H_

#include "stdint.h"
#include "stdbool.h"

#define BYTE_BIT_SIZE	8
#define SHORT_BIT_SIZE	16
#define LONG_BIT_SIZE	32

#define BIT0			(0x1 << 0 )
#define BIT1			(0x1 << 1 )
#define BIT2			(0x1 << 2 )
#define BIT3			(0x1 << 3 )
#define BIT4			(0x1 << 4 )
#define BIT5			(0x1 << 5 )
#define BIT6			(0x1 << 6 )
#define BIT7			(0x1 << 7 )
#define BIT8			(0x1 << 8 )
#define BIT9			(0x1 << 9 )
#define BIT10			(0x1 << 10)
#define BIT11			(0x1 << 11)
#define BIT12			(0x1 << 12)
#define BIT13			(0x1 << 13)
#define BIT14			(0x1 << 14)
#define BIT15			(0x1 << 15)
#define BIT16			(0x1 << 16)
#define BIT17			(0x1 << 17)
#define BIT18			(0x1 << 18)
#define BIT19			(0x1 << 19)
#define BIT20			(0x1 << 20)
#define BIT21			(0x1 << 21)
#define BIT22			(0x1 << 22)
#define BIT23			(0x1 << 23)
#define BIT24			(0x1 << 24)
#define BIT25			(0x1 << 25)
#define BIT26			(0x1 << 26)
#define BIT27			(0x1 << 27)
#define BIT28			(0x1 << 28)
#define BIT29			(0x1 << 29)
#define BIT30			(0x1 << 30)
#define BIT31			(0x1 << 31)

typedef enum {
	Byte, Short, Long
} data_type_t;

#define NUM_TO_BIT_NUM(X)		(X-1)
#define NUM_TO_BIT(X)			(0x1 << X)
#define IS_IN_RANGE(X,L,H)		( ( (X <= L) || (H < X) ) ? 0 : 1 ) // the range set is (L,H) not [L,H]

__inline uint8_t inline_NumToBitNum(int8_t num, data_type_t type) {
	uint8_t maxNumOfBits = 0;
	switch (type) {
	case Byte:
		maxNumOfBits = BYTE_BIT_SIZE;
		break;
	case Short:
		maxNumOfBits = SHORT_BIT_SIZE;
		break;
	case Long:
		maxNumOfBits = LONG_BIT_SIZE;
		break;
	}
	if (num <= 0) {
		return 0;
	}
	if (maxNumOfBits < num) {
		return maxNumOfBits - 1;
	}
	return NUM_TO_BIT_NUM(num);
}

__inline bool inline_IsUnsignedInRange(uint32_t num, uint32_t lowBound,
		uint32_t highBound) {
	return IS_IN_RANGE(num, lowBound, highBound);
}

__inline bool inline_IsSignedInRange(int32_t num, int32_t lowBound,
		int32_t highBound) {
	return IS_IN_RANGE(num, lowBound, highBound);
}

#ifdef USE_FLOAT
__inline bool inline_IsInRange(float num, float lowBound, float highBound) {
	return IS_IN_RANGE(num, lowBound, highBound);
}
#endif

uint8_t NumToBitNum(int8_t num, data_type_t type);
bool IsUnsignedInRange(uint32_t num, uint32_t lowBound, uint32_t highBound);
bool IsSignedInRange(int32_t num, int32_t lowBound, int32_t highBound);
//uint16_t WordToBitNum(uint16_t num);
//uint32_t LongToBitNum(uint32_t num);

#endif /* PRODUCTION_CODE_UTILS_INC_HELPERS_H_ */
