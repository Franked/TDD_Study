/*
 * CircularBuffer.h
 *
 *  Created on: Dec 6, 2017
 *      Author: Ehud Frank
 */

#ifndef PRODUCTION_CODE_UTILS_INC_CIRCULARBUFFER_H_
#define PRODUCTION_CODE_UTILS_INC_CIRCULARBUFFER_H_

#include "stdint.h"
#include "stdbool.h"
#include "string.h"
/*
 * [0]  [1]  [2]  [3]
 * ---------------------
 * |back|    |    |front|
 * ---------------------
 */

#define CIRC_BUFF_AK		false
#define CIRC_BUFF_NAK		true

typedef struct {
	uint8_t index;
	uint8_t outdex;
	uint8_t count;
	uint8_t * const data;
	const uint8_t length;
} circularBuffer_t;

#define CIRCULAR_BUFF_CREATE(name, size) \
		uint8_t name##Data[size] = {0}; \
		circularBuffer_t name = { \
					.index = 0, \
					.outdex = 0, \
					.count = 0, \
					.data = name##Data, \
					.length = size \
		};

#define CIRCULAR_BUFF_CREATE_STATIC(name, size) \
		static uint8_t name##Data[size] = {0}; \
		static circularBuffer_t name = { \
					.index = 0, \
					.outdex = 0, \
					.count = 0, \
					.data = name##Data, \
					.length = size \
		};

bool CircularBuffer_Push(circularBuffer_t * circBuff, uint8_t data);
uint8_t CircularBuffer_Pop(circularBuffer_t * circBuff);
void CircularBuffer_Clear(circularBuffer_t * circBuff);
bool CircularBuffer_IsEmpty(circularBuffer_t * circBuff);
bool CircularBuffer_IsFull(circularBuffer_t * circBuff);
#endif /* PRODUCTION_CODE_UTILS_INC_CIRCULARBUFFER_H_ */
