/*
 * HelpersTestRunner.c
 *
 *  Created on: Dec 8, 2017
 *      Author: Ehud Frank
 */

#include "unity_fixture.h"

TEST_GROUP_RUNNER(Helpers) {
	RUN_TEST_CASE(Helpers, numToBitNumFunctionality);
	puts("numToBitNumFunctionality");
	RUN_TEST_CASE(Helpers, inline_numToBitNumFunctionality);
	puts("inline_numToBitNumFunctionality");
	RUN_TEST_CASE(Helpers, inline_numToBitNumCheckBounds);
	puts("inline_numToBitNumCheckBounds");
	RUN_TEST_CASE(Helpers, isInRangeFunctionality);
	puts("isInRangeFunctionality");
	RUN_TEST_CASE(Helpers, inline_IsUnsignedInRangeFunctionality);
	puts("inline_IsUnsignedInRangeFunctionality");
	RUN_TEST_CASE(Helpers, inline_IsSignedInRangeFunctionality);
	puts("inline_IsSignedInRangeFunctionality");
}
