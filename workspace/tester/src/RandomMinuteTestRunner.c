/*
 * RandomMinuteTestRunner.c
 *
 *  Created on: Dec 18, 2017
 *      Author: Ehud Frank
 */

#include "unity_fixture.h"

TEST_GROUP_RUNNER(RandomMinute) {
	RUN_TEST_CASE(RandomMinute, GetIsInRange);
	puts("GetIsInRange");

	RUN_TEST_CASE(RandomMinute, AllValuesPossible);
	puts("AllValuesPossible");
}

