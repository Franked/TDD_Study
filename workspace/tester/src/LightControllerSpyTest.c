/*
 * LightSchedulerTest.c
 *
 *  Created on: Dec 14, 2017
 *      Author: Ehdu Frank
 */

#include "unity_fixture.h"
#include "LightControllerSpy.h"

//static void checkLightState(int id, int level) {
//	if (id == LIGHT_ID_UNKNOWN) {
//		LONGS_EQUAL(id, LightControllerSpy_GetLastId());
//		LONGS_EQUAL(level, LightControllerSpy_GetLastState());
//	} else
//		LONGS_EQUAL(level, LightControllerSpy_GetLightState(id));
//}

TEST_GROUP(LightControllerSpy);

TEST_SETUP(LightControllerSpy) {
	LightController_Create();
}

TEST_TEAR_DOWN(LightControllerSpy) {
	LightController_Destroy();
}

TEST(LightControllerSpy, Create) {
	LONGS_EQUAL(LIGHT_ID_UNKNOWN, LightControllerSpy_GetLastId());
	LONGS_EQUAL(LIGHT_STATE_UNKNOWN, LightControllerSpy_GetLastState());
}

TEST(LightControllerSpy, RememberTheLastLightIdControlled) {
	LightController_On(10);
	LONGS_EQUAL(10, LightControllerSpy_GetLastId());
	LONGS_EQUAL(LIGHT_ON, LightControllerSpy_GetLastState());
}

TEST(LightControllerSpy, RememberAllLightStates) {
	LightController_On(0);
	LightController_Off(31);
	LONGS_EQUAL(LIGHT_ON, LightControllerSpy_GetLightState(0));
	LONGS_EQUAL(LIGHT_OFF, LightControllerSpy_GetLightState(31));
}
