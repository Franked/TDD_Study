/*
 * ledDriverTest.c
 *
 *  Created on: Dec 5, 2017
 *      Author: Ehud Frank (based on TDD b book)
 */
#include "unity_fixture.h"
#include "RuntimeErrorStub.h"
#include "LedDriver.h"

static uint16_t virtualLeds;

TEST_GROUP(LedDriver);

TEST_SETUP(LedDriver) {
	LedDriver_Create(&virtualLeds);
}

TEST_TEAR_DOWN(LedDriver) {
}

TEST(LedDriver, LedsOffAfterCreate) {
	virtualLeds = 0xFFFF;
	LedDriver_Create(&virtualLeds);
	TEST_ASSERT_EQUAL_HEX16(0, virtualLeds);
}

TEST(LedDriver, TurnOnLedOne) {
	LedDriver_TurnOn(1);
	TEST_ASSERT_EQUAL_HEX16(1, virtualLeds);
}

TEST(LedDriver, TurnOffLedOne) {
	LedDriver_TurnOn(1);
	LedDriver_TurnOff(1);
	TEST_ASSERT_EQUAL_HEX16(0, virtualLeds);
}

TEST(LedDriver, TurnOnMultipleLeds) {
	LedDriver_TurnOn(9);
	LedDriver_TurnOn(8);
	TEST_ASSERT_EQUAL_HEX16(0x180, virtualLeds);
}

TEST(LedDriver, TurnOffAnyLed) {
	LedDriver_TurnAllOn();
	LedDriver_TurnOff(8);
	TEST_ASSERT_EQUAL_HEX16(0xFF7F, virtualLeds);
}

TEST(LedDriver, AllOn) {
	LedDriver_TurnAllOn();
	TEST_ASSERT_EQUAL_HEX16(0xffff, virtualLeds);
}

TEST(LedDriver, AllOff) {
	LedDriver_TurnAllOn();
	LedDriver_TurnAllOff();
	TEST_ASSERT_EQUAL_HEX16(0, virtualLeds);
}

TEST(LedDriver, OutOfBoundsChangesNothing) {
	LedDriver_TurnAllOn();
	LedDriver_TurnOn(-1);
	LedDriver_TurnOn(0);
	LedDriver_TurnOff(17);
	LedDriver_TurnOff(3141);
	TEST_ASSERT_EQUAL_HEX16(UINT16_MAX, virtualLeds);
}

TEST(LedDriver, OutOfBoundsProducesRuntimeError) {
	LedDriver_TurnOn(-1);
	TEST_ASSERT_EQUAL_STRING("LED Driver: out-of-bounds LED",
			RuntimeErrorStub_GetLastError());
	TEST_ASSERT_EQUAL(-1, (int16_t )RuntimeErrorStub_GetLastParameter());
}
