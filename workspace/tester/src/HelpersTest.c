/*
 * HelpersTest.c
 *
 *  Created on: Dec 8, 2017
 *      Author: Ehud Frank
 */

#include "unity_fixture.h"
#include "helpers.h"

TEST_GROUP(Helpers);

TEST_SETUP(Helpers) {

}

TEST_TEAR_DOWN(Helpers) {

}

// TODO helpers test list
/*
 * + NUM_TO_BIT_NUM macro:
 *		- check basic functionality (macro can't check bounds!!!)
 * + IS_IN_RANGE macro:
 *		- check basic functionality-
 * 			* when in range, return 1 and not return 0
 * + inline_IsUnsignedInRange inline function
 *		- check basic functionality (inputs are uint32_t type so auto up-cast lower input types)
 *			* when in range, return 1 and not return 0
 * + inline_IsSignedInRange inline function
 *		- check basic functionality (inputs are int32_t type so auto up-cast lower input types)
 *			* when in range, return 1 and not return 0
 * + inline_numToByteBitNum inline function
 *		- check basic functionality
 *		- check bounds
 * */
TEST(Helpers, numToBitNumFunctionality) {
	TEST_ASSERT_EQUAL(1, NUM_TO_BIT_NUM(2));
	TEST_ASSERT_EQUAL(2, NUM_TO_BIT_NUM(3));
}

TEST(Helpers, inline_numToBitNumFunctionality) {
	// byte
	TEST_ASSERT_EQUAL(1, inline_NumToBitNum(2, Byte));
	TEST_ASSERT_EQUAL(2, inline_NumToBitNum(3, Byte));
	// Short
	TEST_ASSERT_EQUAL(9, inline_NumToBitNum(10, Short));
	// Long
	TEST_ASSERT_EQUAL(19, inline_NumToBitNum(20, Long));
}

TEST(Helpers, inline_numToBitNumCheckBounds) {
	// byte
	TEST_ASSERT_EQUAL(0, inline_NumToBitNum(0, Byte));
	TEST_ASSERT_EQUAL(7, inline_NumToBitNum(9, Byte));
	// Short
	TEST_ASSERT_EQUAL(0, inline_NumToBitNum(-1, Short));
	TEST_ASSERT_EQUAL(15, inline_NumToBitNum(16, Short));
	TEST_ASSERT_EQUAL(15, inline_NumToBitNum(17, Short));
	// Long
	TEST_ASSERT_EQUAL(31, inline_NumToBitNum(32, Long));
	TEST_ASSERT_EQUAL(31, inline_NumToBitNum(33, Long));
}

TEST(Helpers, isInRangeFunctionality) {
	// in range
	TEST_ASSERT_EQUAL(1, IS_IN_RANGE(2,-1,3));
	// below range
	TEST_ASSERT_EQUAL(0, IS_IN_RANGE(0,1,3));
	// above range
	TEST_ASSERT_EQUAL(0, IS_IN_RANGE(4,1,3));
}

TEST(Helpers, inline_IsUnsignedInRangeFunctionality) {
	// in range
	TEST_ASSERT_EQUAL(1, inline_IsUnsignedInRange(2,1,3));
	// below range
	TEST_ASSERT_EQUAL(0, inline_IsUnsignedInRange(0,1,3));
	// above range
	TEST_ASSERT_EQUAL(0, inline_IsUnsignedInRange(4,1,3));

	uint8_t num = 2, lowBound = 1, highBound = 3;
	TEST_ASSERT_EQUAL(1, inline_IsUnsignedInRange(num,lowBound,highBound));
}

TEST(Helpers, inline_IsSignedInRangeFunctionality) {
	// in range
	TEST_ASSERT_EQUAL(1, inline_IsSignedInRange(-2,-10,3));
	// below range
	TEST_ASSERT_EQUAL(0, inline_IsSignedInRange(-2,-2,3));
	// above range
	TEST_ASSERT_EQUAL(0, inline_IsSignedInRange(4,1,3));

	int8_t num = 2, lowBound = -1, highBound = 3;
	TEST_ASSERT_EQUAL(1, inline_IsSignedInRange(num,lowBound,highBound));
}
