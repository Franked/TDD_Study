/*
 * FakeTimeServiceTestRunner.c
 *
 *  Created on: Dec 14, 2017
 *      Author: Ehud Frank
 */

#include "unity_fixture.h"
#include "FakeTimeService.h"

TEST_GROUP_RUNNER(FakeTimeService) {
	RUN_TEST_CASE(FakeTimeService, Create);
	puts("Create");
	RUN_TEST_CASE(FakeTimeService, Set);
	puts("Set");
}
