/*
 * FakeTimeServiceTest.c
 *
 *  Created on: Dec 14, 2017
 *      Author: Ehud Frank
 */

#include "unity_fixture.h"
#include "FakeTimeService.h"

Time time ;
TEST_GROUP(FakeTimeService);

TEST_SETUP(FakeTimeService) {
	TimeService_Create(&time);
}
TEST_TEAR_DOWN(FakeTimeService) {

}
TEST(FakeTimeService, Create) {
	TimeService_GetTime(&time);
	LONGS_EQUAL(TIME_UNKNOWN, time.minuteOfDay);
	LONGS_EQUAL(TIME_UNKNOWN, time.dayOfWeek);
}
TEST(FakeTimeService, Set) {
	FakeTimeService_SetMinute(42);
	FakeTimeService_SetDay(SATURDAY);
	TimeService_GetTime(&time);
	LONGS_EQUAL(42, time.minuteOfDay);
	LONGS_EQUAL(SATURDAY, time.dayOfWeek);
}
