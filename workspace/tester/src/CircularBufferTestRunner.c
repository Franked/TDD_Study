/*
 * CircularBufferTestRunner.c
 *
 *  Created on: Dec 6, 2017
 *      Author: Ehud Frank
 */

#include "unity_fixture.h"

// TODO: Circular Buffer Tests list:
/*
 * uint8_t buffer functions:
 * 1. void CircularBuffer_In(uint8_t * buff) function
 * 		a.
 */
TEST_GROUP_RUNNER(CircularBuffer) {
	RUN_TEST_CASE(CircularBuffer, bufferCreateInitToZero);
	puts("bufferCreateInitToZero");
	RUN_TEST_CASE(CircularBuffer, pushDataToBuffer);
	puts("pushDataToBuffer");
	RUN_TEST_CASE(CircularBuffer, popDataFromBuffer);
	puts("popDataFromBuffer");
	RUN_TEST_CASE(CircularBuffer, clearBuffer);
	puts("clearBuffer");
	RUN_TEST_CASE(CircularBuffer, isEmptyBuffer);
	puts("isEmptyBuffer");
	RUN_TEST_CASE(CircularBuffer, popDataWhenBufferEmpty);
	puts("popDataWhenBufferEmpty");
	RUN_TEST_CASE(CircularBuffer, fillBuffer);
	puts("fillBuffer");
	RUN_TEST_CASE(CircularBuffer, isFullBuffer);
	puts("isFullBuffer");
	RUN_TEST_CASE(CircularBuffer, pushDataWhenBufferFull);
	puts("pushDataWhenBufferFull");
}
