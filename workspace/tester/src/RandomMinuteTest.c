/*
 * RandomMinuteTest.c
 *
 *  Created on: Dec 18, 2017
 *      Author: Ehud Frank
 */

#include "unity_fixture.h"
#include "RandomMinute.h"

#include "string.h"

enum {
	BOUND = 30
};

int minute;

static void AssertMinuteIsInRange() {
	if (minute < -BOUND || minute > BOUND) {
		printf("bad minute value: %d\n", minute);
		FAIL("Minute out of range");
	}
}
TEST_GROUP(RandomMinute);

TEST_SETUP(RandomMinute) {
	RandomMinute_Create(BOUND);
	srand(1);
}
TEST_TEAR_DOWN(RandomMinute) {

}

TEST(RandomMinute, GetIsInRange) {
	for (int i = 0; i < 100; i++) {
		minute = RandomMinute_Get();
		AssertMinuteIsInRange();
	}
}

TEST(RandomMinute, AllValuesPossible) {
	int hit[2 * BOUND + 1];
	memset(hit, 0, sizeof(hit));
	int i;
	for (i = 0; i < 225; i++) {
		minute = RandomMinute_Get();
		AssertMinuteIsInRange();
		hit[minute + BOUND]++;
	}
	for (i = 0; i < 2 * BOUND + 1; i++) {
		CHECK(hit[i] > 0);
	}
}

/*
 *
 *
 *
 *
 *
 */

