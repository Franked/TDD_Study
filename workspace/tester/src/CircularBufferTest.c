/*
 * CircularBufferTest.c
 *
 *  Created on: Dec 6, 2017
 *      Author: Ehud frank
 */

#include "unity_fixture.h"
#include "CircularBuffer.h"

// TODO: test list for circular buffer
/*	1. create a circular buffer instance.
 * 	2. push data to buffer
 * 	3. pop data from buffer.
 * 	4. clear the buffer
 * 	5. when buffer is clear check isEmpty function
 * 	6. pop from buffer when buffer is empty
 * 	6. fill all the buffer
 * 	7. push to buffer when buffer is full
 * 	8. when buffer filled check isFull
 * 	9. check get count (how many items are in buffer).
 * */
TEST_GROUP(CircularBuffer);

#define SIZE_OF_MOCK_CIRC_BUFF	128

CIRCULAR_BUFF_CREATE_STATIC(circBuff1, SIZE_OF_MOCK_CIRC_BUFF);

TEST_SETUP(CircularBuffer) {
}

TEST_TEAR_DOWN(CircularBuffer) {
}

TEST(CircularBuffer, bufferCreateInitToZero) {
	int i;
	for (i = 0; i < SIZE_OF_MOCK_CIRC_BUFF; i++) {
		TEST_ASSERT_EQUAL_UINT8(0, circBuff1.data[i]);
	}
	TEST_ASSERT_EQUAL_UINT8(0, circBuff1.count);
	TEST_ASSERT_EQUAL_UINT8(circBuff1.index, circBuff1.outdex);
}

TEST(CircularBuffer, pushDataToBuffer) {
	CircularBuffer_Push(&circBuff1, 2);
	TEST_ASSERT_EQUAL(2, circBuff1.data[0]);
}

TEST(CircularBuffer, popDataFromBuffer) {
	TEST_ASSERT_EQUAL(2, CircularBuffer_Pop(&circBuff1));
}

TEST(CircularBuffer, clearBuffer) {
	CircularBuffer_Clear(&circBuff1);
	TEST_ASSERT_EQUAL(0, circBuff1.count);
	TEST_ASSERT_EQUAL(circBuff1.index, circBuff1.outdex);
}

TEST(CircularBuffer, isEmptyBuffer) {
	TEST_ASSERT_EQUAL(true, CircularBuffer_IsEmpty(&circBuff1));
}

TEST(CircularBuffer, popDataWhenBufferEmpty) {
	TEST_ASSERT_EQUAL(circBuff1.data[circBuff1.outdex],
			CircularBuffer_Pop(&circBuff1));
}

TEST(CircularBuffer, fillBuffer) {
	int i;
	for (i = 0; i < SIZE_OF_MOCK_CIRC_BUFF; i++) {
		CircularBuffer_Push(&circBuff1, i);
	}
	TEST_ASSERT_EQUAL(circBuff1.length, circBuff1.count);
	TEST_ASSERT_EQUAL(circBuff1.length, circBuff1.index);
	TEST_ASSERT_EQUAL_UINT8(0, circBuff1.outdex);
	for (i = 0; i < SIZE_OF_MOCK_CIRC_BUFF; i++) {
		TEST_ASSERT_EQUAL(i, circBuff1.data[i]);
	}
}

TEST(CircularBuffer, pushDataWhenBufferFull) {
	TEST_ASSERT_EQUAL(CIRC_BUFF_NAK, CircularBuffer_Push(&circBuff1, 2));
	TEST_ASSERT_EQUAL((SIZE_OF_MOCK_CIRC_BUFF - 1),
			circBuff1.data[(SIZE_OF_MOCK_CIRC_BUFF - 1)]);
}

TEST(CircularBuffer, isFullBuffer) {
	TEST_ASSERT_EQUAL(true, CircularBuffer_IsFull(&circBuff1));
}

