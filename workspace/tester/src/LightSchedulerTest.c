/*
 * LightSchedulerTest.c
 *
 *  Created on: Dec 14, 2017
 *      Author: Ehud Frank
 */

#include "unity_fixture.h"
#include "LightControllerSpy.h"
#include "FakeTimeService.h"
#include "LightScheduler.h"
#include "FakeRandomMinute.h"

TEST_GROUP(LightScheduler);

TEST_SETUP(LightScheduler) {
	LightController_Create();
	LightScheduler_Create();
}

TEST_TEAR_DOWN(LightScheduler) {
	LightScheduler_Destroy();
	LightController_Destroy();
}

static void setTime(int day, int minuteOfDay) {
	FakeTimeService_SetDay(day);
	FakeTimeService_SetMinute(minuteOfDay);
}
static void checkLightState(int id, int level) {
	if (id == LIGHT_ID_UNKNOWN) {
		LONGS_EQUAL(id, LightControllerSpy_GetLastId());
		LONGS_EQUAL(level, LightControllerSpy_GetLastState());
	} else
		LONGS_EQUAL(level, LightControllerSpy_GetLightState(id));
}

TEST(LightScheduler, NoScheduleNothingHappens) {
	setTime(MONDAY, 100);
	LightScheduler_Wakeup();
	checkLightState(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, ScheduleOnEverydayNotTimeYet) {
	LightScheduler_ScheduleTurnOn(3, EVERYDAY, 1200);
	setTime(MONDAY, 1199);
	LightScheduler_Wakeup();
	checkLightState(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, ScheduleOnEverydayItIsTime) {
	LightScheduler_ScheduleTurnOn(3, EVERYDAY, 1200);
	setTime(MONDAY, 1200);
	LightScheduler_Wakeup();
	checkLightState(3, LIGHT_ON);
}

TEST(LightScheduler, ScheduleOffEverydayItIsTime) {
	LightScheduler_ScheduleTurnOff(3, EVERYDAY, 1200);
	setTime(MONDAY, 1200);
	LightScheduler_Wakeup();
	checkLightState(3, LIGHT_OFF);
}

TEST(LightScheduler, ScheduleTuesdayButItsMonday) {
	LightScheduler_ScheduleTurnOn(3, TUESDAY, 1200);
	setTime(MONDAY, 1200);
	LightScheduler_Wakeup();
	checkLightState(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, ScheduleWeekendButItsFriday) {
	LightScheduler_ScheduleTurnOn(3, WEEKEND, 1200);
	setTime(FRIDAY, 1200);
	LightScheduler_Wakeup();
	checkLightState(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, ScheduleWeekEndItsSaturday) {
	LightScheduler_ScheduleTurnOn(3, WEEKEND, 1200);
	setTime(SATURDAY, 1200);
	LightScheduler_Wakeup();
	checkLightState(3, LIGHT_ON);
}

TEST(LightScheduler, ScheduleWeekDayItsTuesday) {
	LightScheduler_ScheduleTurnOn(3, WEEKDAY, 1200);
	setTime(TUESDAY, 1200);
	LightScheduler_Wakeup();
	checkLightState(3, LIGHT_ON);
}

TEST(LightScheduler, ScheduleTwoEventsAtTheSameTIme) {
	LightScheduler_ScheduleTurnOn(3, SUNDAY, 1200);
	LightScheduler_ScheduleTurnOn(12, SUNDAY, 1200);
	setTime(SUNDAY, 1200);
	LightScheduler_Wakeup();
	checkLightState(3, LIGHT_ON);
	checkLightState(12, LIGHT_ON);
}

TEST(LightScheduler, RejectsTooManyEvents) {
	int i;
	for (i = 0; i < 128; i++) {
		LONGS_EQUAL(LS_OK, LightScheduler_ScheduleTurnOn(0, MONDAY, i));
	}
	LONGS_EQUAL(LS_ERROR, LightScheduler_ScheduleTurnOn(0, MONDAY, i));
}

TEST(LightScheduler, RemoveRecyclesScheduleSlot) {
	int i;
	for (i = 0; i < 128; i++) {
		LONGS_EQUAL(LS_OK, LightScheduler_ScheduleTurnOn(0, MONDAY, i));
	}
	LightScheduler_ScheduleRemove();
	LONGS_EQUAL(LS_OK, LightScheduler_ScheduleTurnOn(0, MONDAY, i));
}
/*
 *
 *
 *
 *
 *
 *
 */
/**************************************************************/
TEST_GROUP(LightSchedulerCreation);

TEST_SETUP(LightSchedulerCreation) {
}
TEST_TEAR_DOWN(LightSchedulerCreation) {

}
TEST(LightSchedulerCreation, LightSchedulerRegisterToTimeService) {
	LightScheduler_Create();
	TEST_ASSERT_POINTERS_EQUAL((void* )LightScheduler_Wakeup,
			(void* )FakeTimeService_GetCallback());
	TEST_ASSERT_EQUAL(60, FakeTimeService_GetCallbackPeriod());
}

TEST(LightSchedulerCreation, LightSchedulerUnregisterToTimeService) {
	LightScheduler_Create();
	LightScheduler_Destroy();
	TEST_ASSERT_POINTERS_EQUAL(NULL,
			(void * ) FakeTimeService_GetCallbackPeriod());
	TEST_ASSERT_EQUAL(0, FakeTimeService_GetCallbackPeriod());
}
/*
 *
 *
 *
 *
 *
 *
 *
 */
int (*savedRandomMinute_Get)();

TEST_GROUP(LightSchedulerRandomize);

TEST_SETUP(LightSchedulerRandomize) {
	LightController_Create();
	LightScheduler_Create();
	savedRandomMinute_Get = RandomMinute_Get;
	RandomMinute_Get = FakeRandomMinute_Get;
}

TEST_TEAR_DOWN(LightSchedulerRandomize) {
	LightController_Destroy();
	LightScheduler_Destroy();
	RandomMinute_Get = savedRandomMinute_Get;
}

TEST(LightSchedulerRandomize, TurnsOnEarly) {
	FakeRandomMinute_SetFirstAndIncrement(-10, 5);
	LightScheduler_ScheduleTurnOn(4, EVERYDAY, 600);
	LightScheduler_Randomize(4, EVERYDAY, 600);
	setTime(MONDAY, 600 - 10);
	LightScheduler_Wakeup();
	checkLightState(4, LIGHT_ON);
}
