/*
 * LightControllerSpyTestRunner.c
 *
 *  Created on: Dec 14, 2017
 *      Author: Ehud Frank
 */

#include "unity_fixture.h"

TEST_GROUP_RUNNER(LightControllerSpy) {
	RUN_TEST_CASE(LightControllerSpy, Create);
	puts("Create");

	RUN_TEST_CASE(LightControllerSpy, RememberTheLastLightIdControlled);
	puts("RememberTheLastLightIdControlled");

	RUN_TEST_CASE(LightControllerSpy, RememberAllLightStates);
	puts("RememberAllLightStates");
}
