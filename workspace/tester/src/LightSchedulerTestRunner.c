/*
 * LightSchedulerTestRunner.c
 *
 *  Created on: Dec 15, 2017
 *      Author: Ehud Frank
 */

#include "unity_fixture.h"

TEST_GROUP_RUNNER(LightScheduler) {
	RUN_TEST_CASE(LightScheduler, NoScheduleNothingHappens);
	puts("NoScheduleNothingHappens");

	RUN_TEST_CASE(LightScheduler, ScheduleOnEverydayNotTimeYet);
	puts("ScheduleOnEverydayNotTimeYet");

	RUN_TEST_CASE(LightScheduler, ScheduleOnEverydayItIsTime);
	puts("ScheduleOnEverydayItIsTime");

	RUN_TEST_CASE(LightScheduler, ScheduleOffEverydayItIsTime);
	puts("ScheduleOffEverydayItIsTime");

	RUN_TEST_CASE(LightScheduler, ScheduleTuesdayButItsMonday);
	puts("ScheduleTuesdayButItsMonday");

	RUN_TEST_CASE(LightScheduler, ScheduleWeekendButItsFriday);
	puts("ScheduleWeekendButItsFriday");

	RUN_TEST_CASE(LightScheduler, ScheduleWeekEndItsSaturday);
	puts("ScheduleWeekEndItsSaturday");

	RUN_TEST_CASE(LightScheduler, ScheduleWeekDayItsTuesday);
	puts("ScheduleWeekDayItsTuesday");

	RUN_TEST_CASE(LightScheduler, ScheduleTwoEventsAtTheSameTIme);
	puts("ScheduleTwoEventsAtTheSameTIme");

	RUN_TEST_CASE(LightScheduler, RejectsTooManyEvents);
	puts("RejectsTooManyEvents");

	RUN_TEST_CASE(LightScheduler, RemoveRecyclesScheduleSlot);
	puts("RemoveRecyclesScheduleSlot");
}

/*******************************************************************/
TEST_GROUP_RUNNER(LightSchedulerCreation) {
	RUN_TEST_CASE(LightSchedulerCreation, LightSchedulerRegisterToTimeService);
	puts("LightSchedulerRegisterToTimeService");

	RUN_TEST_CASE(LightSchedulerCreation, LightSchedulerUnregisterToTimeService);
	puts("LightSchedulerUnregisterToTimeService");
}
/*
 *
 *
 *
 *
 */
TEST_GROUP_RUNNER(LightSchedulerRandomize) {
	RUN_TEST_CASE(LightSchedulerRandomize, TurnsOnEarly);
	puts("TurnsOnEarly");
}
