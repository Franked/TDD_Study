/*
 * LedDriverTestRunner.c
 *
 *  Created on: Dec 5, 2017
 *      Author: Ehud Frank
 */

#include "unity_fixture.h"

TEST_GROUP_RUNNER(LedDriver) {
	RUN_TEST_CASE(LedDriver, LedsOffAfterCreate);
	puts("LedsOffAfterCreate");
	RUN_TEST_CASE(LedDriver, TurnOnLedOne);
	puts("TurnOnLedOne");
	RUN_TEST_CASE(LedDriver, TurnOffLedOne);
	puts("TurnOffLedOne");
	RUN_TEST_CASE(LedDriver, TurnOnMultipleLeds);
	puts("TurnOnMultipleLeds");
	RUN_TEST_CASE(LedDriver, TurnOffAnyLed);
	puts("TurnOffAnyLed");
	RUN_TEST_CASE(LedDriver, AllOn);
	puts("AllOn");
	RUN_TEST_CASE(LedDriver, AllOff);
	puts("AllOff");
	RUN_TEST_CASE(LedDriver, OutOfBoundsChangesNothing);
	puts("OutOfBoundsChangesNothing");
	RUN_TEST_CASE(LedDriver, OutOfBoundsProducesRuntimeError)
	puts("OutOfBoundsProducesRuntimeError");
}

