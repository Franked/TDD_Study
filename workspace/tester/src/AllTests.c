/***
 * Excerpted from "Test-Driven Development for Embedded C",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/jgade for more book information.
 ***/
/*- ------------------------------------------------------------------ -*/
/*-    Copyright (c) James W. Grenning -- All Rights Reserved          -*/
/*-    For use by owners of Test-Driven Development for Embedded C,    -*/
/*-    and attendees of Renaissance Software Consulting, Co. training  -*/
/*-    classes.                                                        -*/
/*-                                                                    -*/
/*-    Available at http://pragprog.com/titles/jgade/                  -*/
/*-        ISBN 1-934356-62-X, ISBN13 978-1-934356-62-3                -*/
/*-                                                                    -*/
/*-    Authorized users may use this source code in your own           -*/
/*-    projects, however the source code may not be used to            -*/
/*-    create training material, courses, books, articles, and         -*/
/*-    the like. We make no guarantees that this source code is        -*/
/*-    fit for any purpose.                                            -*/
/*-                                                                    -*/
/*-    www.renaissancesoftware.net james@renaissancesoftware.net       -*/
/*- ------------------------------------------------------------------ -*/

#include "unity_fixture.h"
#include "LedDriver.h"

#if 0 
static void RunAllTests(void)
{
	RUN_TEST_GROUP(LedDriver);
}
#endif 

#if 1
static void RunAllTests(void) {
	puts("");
	puts("\nsprintf:");
	puts("--------------------");
	RUN_TEST_GROUP(sprintf);

	puts("\nLedDriver:");
	puts("--------------------");
	RUN_TEST_GROUP(LedDriver);

	puts("\nCircularBuffer:");
	puts("--------------------");
	RUN_TEST_GROUP(CircularBuffer);
	puts("\nhelpers:");
	puts("--------------------");
	RUN_TEST_GROUP(Helpers);

	puts("\nLightControllerSpy:");
	puts("--------------------");
	RUN_TEST_GROUP(LightControllerSpy);

	puts("\nFakeTimeService:");
	puts("--------------------");
	RUN_TEST_GROUP(FakeTimeService);

	puts("\nLightScheduler:");
	puts("--------------------");
	RUN_TEST_GROUP(LightScheduler);

	puts("\nLightSchedulerCreation:");
	puts("--------------------");
	RUN_TEST_GROUP(LightSchedulerCreation);

	puts("\nRandomMinute:");
	puts("--------------------");
	RUN_TEST_GROUP(RandomMinute);

	puts("\nLightSchedulerRandomize:");
	puts("--------------------");
	RUN_TEST_GROUP(LightSchedulerRandomize);
}
#endif 
#if 0
static void RunAllTests(void)
{
	/*    RUN_TEST_GROUP(unity); */
	RUN_TEST_GROUP(LedDriver);
	RUN_TEST_GROUP(sprintf);
	RUN_TEST_GROUP(UnityFixture);
	RUN_TEST_GROUP(UnityCommandOptions);
	RUN_TEST_GROUP(LeakDetection);
	RUN_TEST_GROUP(FakeTimeService);
	RUN_TEST_GROUP(LightControllerSpy);
	RUN_TEST_GROUP(LightScheduler);
	RUN_TEST_GROUP(LightSchedulerInitAndCleanup);
}
#endif

int main(int argc, const char * argv[]) {
	return UnityMain(argc, argv, RunAllTests);
}
