/*
 * FakeTimeService.h
 *
 *  Created on: Dec 14, 2017
 *      Author: Ehud frank
 */

#ifndef DOUBLES_INC_FAKETIMESERVICE_H_
#define DOUBLES_INC_FAKETIMESERVICE_H_

#include "string.h"
#include "stdint.h"
#include "TimeService.h"
#include "LightScheduler.h"

void TimeService_Create(Time *time);

void FakeTimeService_SetMinute(int32_t min);
void FakeTimeService_SetDay(int day);

void* FakeTimeService_GetCallback(void);
uint32_t FakeTimeService_GetCallbackPeriod(void);

#endif /* DOUBLES_INC_FAKETIMESERVICE_H_ */
