/*
 * FakeRandomMinute.h
 *
 *  Created on: Dec 18, 2017
 *      Author: user
 */

#ifndef DOUBLES_INC_FAKERANDOMMINUTE_H_
#define DOUBLES_INC_FAKERANDOMMINUTE_H_

#include "RandomMinute.h"

int FakeRandomMinute_Get(void);
void FakeRandomMinute_SetFirstAndIncrement(int seed, int increment);


#endif /* DOUBLES_INC_FAKERANDOMMINUTE_H_ */
