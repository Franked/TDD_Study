/*
 * LightControllerSpy.h
 *
 *  Created on: Dec 14, 2017
 *      Author: Ehud Frank
 */

#ifndef DOUBLES_INC_LIGHTCONTROLLERSPY_H_
#define DOUBLES_INC_LIGHTCONTROLLERSPY_H_

#include "LightController.h"

enum {
	LIGHT_ID_UNKNOWN = -1, LIGHT_STATE_UNKNOWN = -1, LIGHT_OFF = 0, LIGHT_ON = 1
};

int LightControllerSpy_GetLightState(int id);
int LightControllerSpy_GetLastId(void);
int LightControllerSpy_GetLastState(void);

#endif /* DOUBLES_INC_LIGHTCONTROLLERSPY_H_ */
