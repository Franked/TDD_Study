/*
 * FakeTimeService.c
 *
 *  Created on: Dec 14, 2017
 *      Author: Ehud frank
 */

#include "FakeTimeService.h"

static Time *m_time;
static TimeServiceTime_t Timer;

void TimeService_Create(Time *time) {
	time->dayOfWeek = TIME_UNKNOWN;
	time->minuteOfDay = TIME_UNKNOWN;
	m_time = time;
}
void TimeService_GetTime(Time *time) {
	time->dayOfWeek = m_time->dayOfWeek;
	time->minuteOfDay = m_time->minuteOfDay;
}

void TimeService_InitTimer(uint32_t period, TimeServiceCallBack_t cb) {
	Timer.cb = cb;
	Timer.period = period;
}

void TimeService_DeinitTimer(uint32_t period, TimeServiceCallBack_t cb) {
	if (cb == Timer.cb && period == Timer.period) {
		Timer.cb = NULL;
		Timer.period = 0;
	}
}

void* FakeTimeService_GetCallback(void) {
	return Timer.cb;
}
uint32_t FakeTimeService_GetCallbackPeriod(void) {
	return Timer.period;
}

void FakeTimeService_SetMinute(int32_t min) {
	m_time->minuteOfDay = min;
}
void FakeTimeService_SetDay(int day) {
	m_time->dayOfWeek = day;
}
