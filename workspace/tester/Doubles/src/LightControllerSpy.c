/*
 * LightControllerSpy.c
 *
 *  Created on: Dec 14, 2017
 *      Author: Ehud Frank
 */

#include "LightControllerSpy.h"

static int LightStates[MAX_NUM_OF_LIGHTS];
static int lastId;
static int lastState;

void LightController_Create(void) {
	lastId = LIGHT_ID_UNKNOWN;
	lastState = LIGHT_STATE_UNKNOWN;
	int i;
	for (i = 0; i < MAX_NUM_OF_LIGHTS; i++) {
		LightStates[i] = LIGHT_STATE_UNKNOWN;
	}
}

void LightController_Destroy(void) {
	int i;
	for (i = 0; i < MAX_NUM_OF_LIGHTS; i++) {
		LightStates[i] = LIGHT_STATE_UNKNOWN;
	}
}

void LightController_On(int id) {
	lastId = id;
	lastState = LIGHT_ON;
	LightStates[id] = LIGHT_ON;
}
void LightController_Off(int id) {
	lastId = id;
	lastState = LIGHT_OFF;
	LightStates[id] = LIGHT_OFF;
}

int LightControllerSpy_GetLightState(int id) {
	return LightStates[id];
}
int LightControllerSpy_GetLastId(void) {
	return lastId;
}
int LightControllerSpy_GetLastState(void) {
	return lastState;
}
